import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class App {
	
	public static void main(String [] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new MainFrame("Tutorial JFrame");
				frame.setVisible(true);
				frame.setSize(500, 500);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
	}
	
}
